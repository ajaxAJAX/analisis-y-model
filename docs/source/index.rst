.. analisis y modelado 2020 documentation master file, created by
   sphinx-quickstart on Sat Dec 19 01:36:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to analisis y modelado 2020's documentation!
====================================================

Proyecto final para la asignatura Arquitectura de Software impartida en la UACM.


.. toctree::
    :maxdepth: 3
    :caption: Contenido:

    data/1-introduccion
    data/2-vision-de-la-solucion
    data/3-alcance
    data/4-contexto-del-sistema
    data/5-informacion-adicional

    



