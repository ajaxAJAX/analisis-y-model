Visión de la solución
=====================

Fase de vision
----------------------------

La implementación del sistema maximizará las operaciones de altas, bajas y cambios.
Con lo que se lograra obtener consultas en tiempo real, y en consecuencia generará menos dependencia de personal involucrado en las áreas encargadas de manejar la información del sistema.  Esperando poder tener indicadores reales que sustenten las decisiones estratégicas para mejorar el rendimiento económico de la empresa.

Características del sistema
--------------------------------

+--------+---------------------------+-----------+----------------------+
|   ID   |        DESCRIPCIÓN        | PRIORIDAD | OBJETIVO DEL NEGOCIO |
+========+===========================+===========+======================+
| CAR-O1 | Gestión de usuarios       | BAJA      | ON-11                |
+--------+---------------------------+-----------+----------------------+
| CAR-02 | Gestión de bajas          | ALTA      | ON-04                |
+--------+---------------------------+-----------+----------------------+
| CAR-03 | Gestión de altas          | ALTA      | ON-03                |
+--------+---------------------------+-----------+----------------------+
| CAR-04 | Gestión de cambios        | MEDIA     | ON-04                |
+--------+---------------------------+-----------+----------------------+
| CAR-05 | Gestión de reportes       | BAJA      | 0N-07                |
+--------+---------------------------+-----------+----------------------+
| CAR-06 | Gestión de disponibilidad | ALTA      | ON-08, ON-09, ON-03  |
+--------+---------------------------+-----------+----------------------+
| CAR-07 | Gestión de búsqueda       | BAJA      | ON-05, ON-10         |
+--------+---------------------------+-----------+----------------------+
| CAR-08 | Respaldo de información   | ALTA      | ON-02, ON-12         |
+--------+---------------------------+-----------+----------------------+
| CAR-09 | Gestión de proveedores    | MEDIA     | ON-01                |
+--------+---------------------------+-----------+----------------------+
| CAR-10 | Gestión de Compras        | MEDIA     | ON-01                |
+--------+---------------------------+-----------+----------------------+
| CAR-11 | Gestión de max y mínimos  | BAJA      | ON-O1                |
+--------+---------------------------+-----------+----------------------+