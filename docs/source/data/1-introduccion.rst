Introducción
===========================================

Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.

Contexto del negocio
----------------------


Antecedentes
-----------------

"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"

Fase del problema
------------------------

"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

Objetivos del negocio
---------------------------

Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Tiene sus raices en una pieza cl´sica de la literatura del Latin, que data del año 45 antes de Cristo, haciendo que este adquiera mas de 2000 años de antiguedad. Richard McClintock, un profesor de Latin de la Universidad de Hampden-Sydney en Virginia, encontró una de las palabras más oscuras de la lengua del latín, "consecteur", en un pasaje de Lorem Ipsum, y al seguir leyendo distintos textos del latín, descubrió la fuente indudable. Lorem Ipsum viene de las secciones 1.10.32 y 1.10.33 de "de Finnibus Bonorum et Malorum" (Los Extremos del Bien y El Mal) por Cicero, escrito en el año 45 antes de Cristo. Este libro es un tratado de teoría de éticas, muy popular durante el Renacimiento. La primera linea del Lorem Ipsum, "Lorem ipsum dolor sit amet..", viene de una linea en la sección 1.10.32


+--------+---------------------------+-----------+----------------------+
|   ID   |        DESCRIPCIÓN        | PRIORIDAD | OBJETIVO DEL NEGOCIO |
+========+===========================+===========+======================+
| CAR-O1 | Gestión de usuarios       | BAJA      | ON-11                |
+--------+---------------------------+-----------+----------------------+
| CAR-02 | Gestión de bajas          | ALTA      | ON-04                |
+--------+---------------------------+-----------+----------------------+
| CAR-03 | Gestión de altas          | ALTA      | ON-03                |
+--------+---------------------------+-----------+----------------------+
| CAR-04 | Gestión de cambios        | MEDIA     | ON-04                |
+--------+---------------------------+-----------+----------------------+
| CAR-05 | Gestión de reportes       | BAJA      | 0N-07                |
+--------+---------------------------+-----------+----------------------+
| CAR-06 | Gestión de disponibilidad | ALTA      | ON-08, ON-09, ON-03  |
+--------+---------------------------+-----------+----------------------+
| CAR-07 | Gestión de búsqueda       | BAJA      | ON-05, ON-10         |
+--------+---------------------------+-----------+----------------------+
| CAR-08 | Respaldo de información   | ALTA      | ON-02, ON-12         |
+--------+---------------------------+-----------+----------------------+
| CAR-09 | Gestión de proveedores    | MEDIA     | ON-01                |
+--------+---------------------------+-----------+----------------------+
| CAR-10 | Gestión de Compras        | MEDIA     | ON-01                |
+--------+---------------------------+-----------+----------------------+
| CAR-11 | Gestión de max y mínimos  | BAJA      | ON-O1                |
+--------+---------------------------+-----------+----------------------+